use std::convert::TryInto;
use std::env;

fn main() {
    let args: Vec<String> = env::args().collect();
    let message = args[1].clone();
    let mut h0: u32 = 0x6a09e667;
    let mut h1: u32 = 0xbb67ae85;
    let mut h2: u32 = 0x3c6ef372;
    let mut h3: u32 = 0xa54ff53a;
    let mut h4: u32 = 0x510e527f;
    let mut h5: u32 = 0x9b05688c;
    let mut h6: u32 = 0x1f83d9ab;
    let mut h7: u32 = 0x5be0cd19;
    let k: [u32; 64] = [
        0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
        0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3, 0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
        0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc, 0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
        0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7, 0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
        0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13, 0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
        0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3, 0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
        0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
        0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208, 0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2
    ];
    let message_bytes = message.as_bytes();
    let data_len = message_bytes.len() as u64;
    let mut data = Vec::new();
    data.extend_from_slice(message_bytes);
    // Append 0b10000000
    data.push(128);
    if (data.len() % 64) != 0 {
        let num_complete_chunks = (data_len + 1) / 64;
        // 512/8=64 bytes - 8 length bytes - byte added to append 0b10000000
        let number_zero_bytes = ((64 * (num_complete_chunks + 1)) - 8) - (data_len + 1);
        // Pad with zeros to achieve a multiple of 512 bits in length
        data.append(vec![0_u8; number_zero_bytes as usize].as_mut());
    }
    // Append data length to data
    data.extend_from_slice(&((data_len) * 8).to_be_bytes());
    let mut chunks = Vec::new();
    let num_complete_chunks = data.len() / 64;
    for i in 0..num_complete_chunks {
        chunks.push(data[(i * 64)..((i + 1) * 64)].to_vec());
    }
    // 64 32-bit words
    let mut message_schedule_array = [0_u32; 64];
    for chunk in chunks {
        for i in 0..16 {
            // Count by 4's
            let expanded_index = i * 4;
            // Copy 4 bytes into the message schedule array as a u32
            message_schedule_array[i] = u32::from_be_bytes(chunk[expanded_index..expanded_index + 4].try_into().unwrap());
        }
        let mut s0;
        let mut s1;
        for i in 16..64 {
            s0 = (message_schedule_array[i - 15].rotate_right(7)) ^ (message_schedule_array[i - 15].rotate_right(18)) ^ (message_schedule_array[i - 15] >> 3);
            s1 = (message_schedule_array[i - 2].rotate_right(17)) ^ (message_schedule_array[i - 2].rotate_right(19)) ^ (message_schedule_array[i - 2] >> 10);
            message_schedule_array[i] = message_schedule_array[i - 16].wrapping_add(s0).wrapping_add(message_schedule_array[i - 7]).wrapping_add(s1);
        }

        let mut a = h0;
        let mut b = h1;
        let mut c = h2;
        let mut d = h3;
        let mut e = h4;
        let mut f = h5;
        let mut g = h6;
        let mut h = h7;

        for i in 0..64 {
            let sigma_1 = (e.rotate_right(6)) ^ (e.rotate_right(11)) ^ (e.rotate_right(25));
            let ch = (e & f) ^ ((!e) & g);
            let temp1 = h.wrapping_add(sigma_1).wrapping_add(ch).wrapping_add(k[i]).wrapping_add(message_schedule_array[i]);
            let sigma_0 = (a.rotate_right(2)) ^ (a.rotate_right(13)) ^ (a.rotate_right(22));
            let maj = (a & b) ^ (a & c) ^ (b & c);
            let temp2 = sigma_0.wrapping_add(maj);

            h = g;
            g = f;
            f = e;
            e = d.wrapping_add(temp1);
            d = c;
            c = b;
            b = a;
            a = temp1.wrapping_add(temp2);
        }

        h0 = h0.wrapping_add(a);
        h1 = h1.wrapping_add(b);
        h2 = h2.wrapping_add(c);
        h3 = h3.wrapping_add(d);
        h4 = h4.wrapping_add(e);
        h5 = h5.wrapping_add(f);
        h6 = h6.wrapping_add(g);
        h7 = h7.wrapping_add(h);
    }
    print!("{:x}", h0);
    print!("{:x}", h1);
    print!("{:x}", h2);
    print!("{:x}", h3);
    print!("{:x}", h4);
    print!("{:x}", h5);
    print!("{:x}", h6);
    println!("{:x}", h7);
}
